# Infubs - Aplicação para escolha inteligente de UBS


## Informações da base de dados

As Unidades Básicas de Saúde (UBS) são a porta de entrada preferencial do Sistema
Único de Saúde (SUS). O objetivo desses postos é atender até 80% dos problemas 
de saúde da população, sem que haja a necessidade de encaminhamento para hospitais

## Objetivo
Analisar as UBS do Brasil e sua qualidade baseando-se nos seguintes critérios:

- Estrutura física e ambiência
- Adaptação de deficientes físicos e idosos
- Equipamentos
- Medicamentos

Tendo foco no nordeste analisando a qualidade e quantidade das mesmas.

Facilitando
o processo de escolha do usuário para qual UBS o mesmo pretende baseando no critério
que lhe tem maior peso.



